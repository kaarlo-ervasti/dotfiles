

# append .local/bin, appimages and flatpak bins to path
export PATH="$PATH":"$HOME"/.local/bin:"$HOME"/appimages:"$HOME"/.local/share/export/bin:/var/lib/flatpak/exports/bin

export XDG_DATA_DIRS="/usr/local/share:/usr/share:/var/lib/flatpak/exports/share:/home/kazzu/.local/share/flatpak/exports/share"

# default programs
export EDITOR="nvim"
export VISUAL="nvim"
export PAGER="less"
export FILE="nnn -dHr"
export TERMINAL="foot"
export BROWSER="librewolf-bin"
export READER="zathura"
export MANPAGER="less"

# ~/ cleanup
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_STATE_HOME="$home/.local/state"
export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export LESSHISTFILE="-"
export WGETRC="${XDG_CONFIG_HOME:-$HOME/.config}/wget/wgetrc"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export WINEPREFIX="${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes/default"
export KODI_DATA="${XDG_DATA_HOME:-$HOME/.local/share/kodi}"
export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/password-store"
export TMUX_TMPDIR="$XDG_RUNTIME_HOME"
export ANDROID_SDK_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/android"
export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"

# set input method modules
export GTK_IM_MODULE=fcitx5
export QT_IM_MODULE=fcitx5
export XMODIFIERS=@im=fcitx5

# wayland
export XDG_CURRENT_DESKTOP=sway
export XDG_DESKTOP_TYPE=wayland
export XDG_SESSION_TYPE=wayland
export ELM_DISPLAY=wl
export QT_QPA_PLATFORM=wayland
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
export QT_QPA_PLATFORMTHEME=qt5ct
export ECORE_EVAS_ENGINE=wayland_egl
export ELM_ENGINE=wayland_egl
export GDK_BACKEND=wayland,x11
export CLUTTER_BACKEND=waylan

# firefox env variables
export MOZ_DBUS_REMOTE=1
export MOZ_ENABLE_WAYLAND=1
export MOZ_DBUS_REMOTE=1
export MOZ_WEBRENDER=1

# set cursor theme and size
export XCURSOR_THEME="Simp1e"
export XCURSOR_SIZE="32"

# fix java
export _JAVA_AWT_WM_NONREPARENTING=1
export _JAVA_OPTIONS='-Dsun.java2d.opengl=true'

# If running from tty1 start sway
if [ "$(tty)" = "/dev/tty1" ]; then
	exec dbus-run-session /usr/bin/sway
fi
