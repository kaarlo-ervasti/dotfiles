# auto-escape urls when pasting
autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic


# History settings
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots) # Include hidden files.

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

### Change opts

# enable AUTO_CD
setopt AUTO_CD

# disable terminal output freezing with ^S
setopt noflowcontrol

# don't share history between shell instances
setopt no_share_history
unsetopt share_history

setopt nohup

# Enable portage completions
autoload -U compinit promptinit
compinit
promptinit; prompt gentoo

# Load zsh-syntax-highlighting; should be last
source /usr/share/zsh/site-contrib/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2> /dev/null
source /usr/share/zsh/site-functions/zsh-autosuggestions.zsh 2> /dev/null
